package com.damozan.iossupersign.utils;

import java.io.File;

import lombok.experimental.UtilityClass;

/**
 * @author Peter.Hong
 * @date 2019/12/14
 */
@UtilityClass
public class FileUtils2 {


    /**
     * @return 后缀名
     */
    public String getSuffixName(File file) {
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }
}
