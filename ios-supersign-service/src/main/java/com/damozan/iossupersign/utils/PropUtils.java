
package com.damozan.iossupersign.utils;
 
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PropUtils extends Properties { 
    private static final long serialVersionUID = 1L;
 
    public PropUtils() {
        super();
    }
 
    public static Resource getByConfType(String confType) {
    	 Resource resource = null;
        try {
           
            String path = PropUtils.class.getClassLoader().getResource("").getPath();
          
            resource = new FileSystemResource(path + confType);
            if (resource.exists()) {
                InputStream is = resource.getInputStream();
                is.close();
            } else { 
                resource = new ClassPathResource(confType);
            }

        } catch (Exception e) { 
            e.printStackTrace();
        }
        
        if(resource!=null) {
        	 try {
				log.info("============config file folder:【" + resource.getURI() + "】");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	 log.info("============config file folder:【" + resource.getDescription()+ "】");
        	 
        }else {
        	log.info("============config file folder:【" + resource + "】");
        }
        System.out.println("============config file folder:【" + resource + "】");
       
        return resource;

    }
 

}
