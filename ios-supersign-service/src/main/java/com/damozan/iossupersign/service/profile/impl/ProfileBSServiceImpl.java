package com.damozan.iossupersign.service.profile.impl;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.damozan.iossupersign.model.bo.AuthorizeBO;
import com.damozan.iossupersign.model.dto.AppleResultDTO;
import com.damozan.iossupersign.model.po.AccountDevicePO;
import com.damozan.iossupersign.service.file.FileService;
import com.damozan.iossupersign.service.profile.ProfileBSService;
import com.damozan.iossupersign.thridparty.appleapi.AppleApiService;

import cn.hutool.core.lang.UUID;

/**
 * @author Peter.Hong
 * @date 2019/12/14
 */
@Service
public class ProfileBSServiceImpl implements ProfileBSService {

    @Value("${file.profileUploadPath}")
    private String profileUploadPath;

    private final AppleApiService appleApiService;

    private final FileService fileService;

    @Autowired
    public ProfileBSServiceImpl(AppleApiService appleApiService, FileService fileService) {
        this.appleApiService = appleApiService;
        this.fileService = fileService;
    }

    @Override
    public String createNewProfile(AccountDevicePO accountDevicePO) {
        AuthorizeBO authorizeBO = AuthorizeBO.builder()
                .iss(accountDevicePO.getIssuerId())
                .p8(accountDevicePO.getP8())
                .kid(accountDevicePO.getKid())
                .build();

        String udid = accountDevicePO.getUdid();

        String pathName = profileUploadPath + accountDevicePO.getKid() +"/"+udid+".mobileprovision";

        File f = new File(pathName);
        if(!f.exists()) {
        	String profilename = UUID.randomUUID().toString().replace("-", "");
        	
        	AppleResultDTO appleResultDTO = appleApiService.getMobileprovision(authorizeBO, accountDevicePO.getBundleIds(),accountDevicePO.getCerId(), accountDevicePO.getDeviceId(), profilename);

            String profileContentBase64 = (String) appleResultDTO.getAttributes().get("profileContent");
            uploadProfile(profileContentBase64, pathName);
        }
         
        return pathName;
    }

    @Override
    public String uploadProfile(String base64, String pathName) {
        File file = fileService.base64ToFile(base64, pathName);
        return file.getAbsolutePath();
    }
}
