package com.damozan.iossupersign.service.device.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.damozan.iossupersign.model.po.AccountDevicePO;
import com.damozan.iossupersign.model.po.DevicePO;
import com.damozan.iossupersign.service.device.DeviceService;
import com.damozan.iossupersign.storage.mysql.mapper.DeviceMapper;

@Service
public class DeviceServiceImpl implements DeviceService {

    private final DeviceMapper deviceMapper;

    public DeviceServiceImpl(DeviceMapper deviceMapper) {
        this.deviceMapper = deviceMapper;
    }

    @Override
    public void insertList(List<DevicePO> devicePOS) {
        deviceMapper.insertList(devicePOS);
    }

    @Override
    public DevicePO getDeviceByUdid(String udid) {
       return deviceMapper.getDeviceByUdid(udid);
    }

    @Override
    public void insert(DevicePO devicePO){
        deviceMapper.insert(devicePO);
    }

    @Override
    public AccountDevicePO getAccountDeviceByUdid(String udid) {
        return deviceMapper.getAccountDeviceByUdid(udid);
    }
}
