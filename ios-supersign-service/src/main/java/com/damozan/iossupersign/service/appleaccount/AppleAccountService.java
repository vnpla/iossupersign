package com.damozan.iossupersign.service.appleaccount;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.damozan.iossupersign.model.po.AppleAccountPO;

public interface AppleAccountService {
    int insert(AppleAccountPO appleAccountPO);

    AppleAccountPO getAccountByAccount(AppleAccountPO appleAccountPO);

    Page<AppleAccountPO> selectAppleAccountByCondition(Page<AppleAccountPO> page, AppleAccountPO appleAccountPO);

    AppleAccountPO getAccountById(Long id);

    List<AppleAccountPO> selectEnableAppleAccounts(Integer deviceLimit, Integer sizeLimit);

    void updateAccountDeviceCount(String account, Integer deviceCount);

    void updateAccountP12Path(String account, String p12Path);
}
