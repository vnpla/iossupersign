package com.damozan.iossupersign.service.device;

import java.util.List;

import com.damozan.iossupersign.model.po.AccountDevicePO;
import com.damozan.iossupersign.model.po.DevicePO;

public interface DeviceService {
    void insertList(List<DevicePO> devicePOS);

    DevicePO getDeviceByUdid(String udid);

    void insert(DevicePO devicePO);

    AccountDevicePO getAccountDeviceByUdid(String udid);
}
