package com.damozan.iossupersign.service.udid;

import javax.transaction.Transactional;

import com.damozan.iossupersign.model.po.IpaPackagePO;

public interface UDIDBSService {

    @Transactional
    Boolean bindUdidToAppleAccount(String udid);

    String getMobileConfig(IpaPackagePO ipaPackagePO);

    String uploadMobileConfig(IpaPackagePO ipaPackagePO);
}
