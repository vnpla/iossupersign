package com.damozan.iossupersign.service.appleaccount;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.damozan.iossupersign.model.po.AppleAccountPO;

public interface AppleAccountBSService {
	
    @Transactional
    void addAppleAccount(AppleAccountPO appleAccountPO);

    Page<AppleAccountPO> selectAppleAccountByCondition(@NotNull Integer currentPage, AppleAccountPO appleAccountPO);

    AppleAccountPO getAccountByAccount(AppleAccountPO appleAccountPO);

    List<AppleAccountPO> selectBestAppleAccount();

    void updateAccountDeviceCount(String account, Integer deviceCount);

    Long addAppleDeviceCountToRedis(String appleAccount, Long deviceCount);

    void uploadP12(MultipartFile p12File, String account);
    
    AppleAccountPO getAccountById(Long id);
    
}
