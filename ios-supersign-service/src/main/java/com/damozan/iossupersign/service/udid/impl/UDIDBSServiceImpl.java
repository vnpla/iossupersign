package com.damozan.iossupersign.service.udid.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.damozan.iossupersign.cache.RedisCache;
import com.damozan.iossupersign.config.DomainConfig;
import com.damozan.iossupersign.model.bo.AuthorizeBO;
import com.damozan.iossupersign.model.bo.UdidBO;
import com.damozan.iossupersign.model.dto.AppleResultDTO;
import com.damozan.iossupersign.model.po.AppleAccountPO;
import com.damozan.iossupersign.model.po.DevicePO;
import com.damozan.iossupersign.model.po.IpaPackagePO;
import com.damozan.iossupersign.service.appleaccount.AppleAccountBSService;
import com.damozan.iossupersign.service.device.DeviceBSService;
import com.damozan.iossupersign.service.file.FileService;
import com.damozan.iossupersign.service.udid.UDIDBSService;
import com.damozan.iossupersign.thridparty.appleapi.AppleApiService;
import com.damozan.iossupersign.utils.FileUtils;
import com.damozan.iossupersign.utils.IosUrlUtils;
import com.damozan.iossupersign.utils.PropUtils;
import com.damozan.iossupersign.utils.ShellUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UDIDBSServiceImpl implements UDIDBSService {

    private final String mobileConfigPath = "udid.mobileconfig";

    private String udidTemplate;

    private Integer deviceLimit = 100;

    private final RedisCache redisCache;

    private final AppleAccountBSService appleAccountBSService;

    private final AppleApiService appleApiService;

    private final DeviceBSService deviceBSService;

    private final DomainConfig domainConfig;

    private final FileService fileService;

    @Value("${file.mobileconfigUploadPath}")
    private String mobileconfigUploadPath;

    @Value("${shPath}")
    private String shPath;

    @Autowired
    public UDIDBSServiceImpl(RedisCache redisCache, AppleAccountBSService appleAccountBSService, AppleApiService appleApiService, DeviceBSService deviceBSService, DomainConfig domainConfig, FileService fileService) {
        this.redisCache = redisCache;
        this.appleAccountBSService = appleAccountBSService;
        this.appleApiService = appleApiService;
        this.deviceBSService = deviceBSService;
        this.domainConfig = domainConfig;
        this.fileService = fileService;
    }

    @PostConstruct
    public void initUDIDTemplate() throws URISyntaxException, IOException {
  
        udidTemplate =  new String( FileUtils.readByte(mobileconfigUploadPath + mobileConfigPath)  );
    }

    @Override
    public String getMobileConfig(IpaPackagePO ipaPackagePO){
        UdidBO udidBO =  UdidBO.builder().payloadDisplayName(ipaPackagePO.getName()).payloadUUID(UUID.randomUUID().toString().replace("-", "")).build();
        
        log.info("getMobileConfig PayloadDisplayName:", udidBO.getPayloadDisplayName());
        
        
        String nowUdidTemplate = udidTemplate
                .replace("@@getUDIDURL", IosUrlUtils.getUDIDUrl(domainConfig.getOpenapiUrlPath(),"/udid/getUDID/",ipaPackagePO.getIpaDownloadId()))
                .replace("@@PayloadDisplayName",Objects.toString(udidBO.getPayloadDisplayName(),udidBO.getPayloadDisplayName()))
                .replace("@@PayloadUUID",udidBO.getPayloadUUID());

        String fileName = "temp-udid-"+ipaPackagePO.getIpaDownloadId() + ".mobileconfig";
        
        String filePath = mobileconfigUploadPath+"/"+ipaPackagePO.getIpaDownloadId()+"/"+fileName;
        try {
            fileService.uploadFile(nowUdidTemplate.getBytes(), filePath);
        } catch (IOException e) {
            log.error("getMobileConfig error", e);
        }
        
        log.info("getMobileConfig filePath:", filePath);
        
        return filePath;
    }

    @Override
    public String uploadMobileConfig(IpaPackagePO ipaPackagePO) {
        String path = getMobileConfig(ipaPackagePO);
        
        return path;
    }

    /**
     * 分配Device到苹果帐号上
     * @param udid
     */
    @Transactional
    @Override
    public Boolean bindUdidToAppleAccount(String udid){
        Boolean isBindSuccess = true; 
        return isBindSuccess;
    }



}