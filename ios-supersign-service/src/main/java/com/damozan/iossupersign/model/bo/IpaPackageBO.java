package com.damozan.iossupersign.model.bo;

import org.springframework.web.multipart.MultipartFile;

import lombok.Builder;
import lombok.Data;

/**
 * @author Peter.Hong
 * @date 2019/12/13
 */
@Data
@Builder
public class IpaPackageBO {

    private Long id;

    private MultipartFile file;

    private String summary;
}
