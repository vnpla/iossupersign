package com.damozan.iossupersign.model.dto;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppleApiResult<T> implements Serializable {

    private T data;

    private Map<String,Object> links;

    private Map<String,Object> meta;
}