package com.damozan.iossupersign.enums;

import org.springframework.http.HttpMethod;

public enum AppleApiEnum {
	  /**
	   * https://developer.apple.com/documentation/appstoreconnectapi
	   */
	
    /**
     * "Deviced-API"
     */
    LIST_DEVICE_API("https://api.appstoreconnect.apple.com/v1/devices",HttpMethod.GET),
    
    REGISTER_NEW_DEVICE_API("https://api.appstoreconnect.apple.com/v1/devices",HttpMethod.POST),
    /**
     * BundelId-API
     */
    REGISTER_NEW_BUNDLEID_API("https://api.appstoreconnect.apple.com/v1/bundleIds",HttpMethod.POST),
    
    LIST_BUNDLEID_API("https://api.appstoreconnect.apple.com/v1/bundleIds",HttpMethod.GET),
    
    //https://api.appstoreconnect.apple.com/v1/bundleIds/{id}
    DELETE_BUNDLEID_API("https://api.appstoreconnect.apple.com/v1/bundleIds/",HttpMethod.DELETE),

    /**
     * Profile-API
     */
    CREATE_PROFILE_API("https://api.appstoreconnect.apple.com/v1/profiles", HttpMethod.POST),
    
//    DELETE https://api.appstoreconnect.apple.com/v1/profiles/{id}

    DELETE_PROFILE_API("https://api.appstoreconnect.apple.com/v1/profiles/{id}", HttpMethod.DELETE),
    	
    /**
     * https://developer.apple.com/documentation/appstoreconnectapi/delete_a_profile#url
     * DELETE https://api.appstoreconnect.apple.com/v1/profiles/{id}
     */
    
    /**
     * Certificates-API
     */
    NEW_CERTIFICATES_API("https://api.appstoreconnect.apple.com/v1/certificates", HttpMethod.POST),
    
    LIST_CERTIFICATES_API("https://api.appstoreconnect.apple.com/v1/certificates", HttpMethod.GET),
    
    /**
     *  GET https://api.appstoreconnect.apple.com/v1/certificates/{id}
     */
   
    	
    //已经被苹果禁止使用
    //DELETE_CERTIFICATES_API("https://api.appstoreconnect.apple.com/v1/certificates/{id}",HttpMethod.DELETE),
    ;


    private String apiPath;

    private HttpMethod httpMethod;

    AppleApiEnum(String apiPath,HttpMethod httpMethod) {

        this.apiPath = apiPath;
        this.httpMethod = httpMethod;
    }

    public String getApiPath(){
        return apiPath;
    }

    public HttpMethod getHttpMethod(){
        return httpMethod;
    }
}
