package com.damozan.iossupersign.storage.mysql.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.damozan.iossupersign.model.po.AccountDevicePO;
import com.damozan.iossupersign.model.po.DevicePO;

/**
 * @author Ami.Tsai
 * @date 2019/12/10
 */
@Mapper
@Repository
public interface DeviceMapper extends BaseMapper<DevicePO> {

    void insertList(@Param("devicePOS") List<DevicePO> devicePOS);

    DevicePO getDeviceByUdid(String udid);

    AccountDevicePO getAccountDeviceByUdid(String udid);
}
